from redisbloom.client import Client
from redis.exceptions import ResponseError

rb = Client(host='0.0.0.0', port=6379)
key = 'testDecay'

try:
    rb.cmsInitByDim(key, 1000, 5)
except ResponseError:
    rb.delete(key)
    rb.cmsInitByDim(key, 1000, 5)

rb.cmsIncrBy(key, ['foo'], [21])
rb.cmsIncrBy(key, ['foo', 'bar'], [5, 15])
print(rb.cmsQuery(key, 'foo', 'bar'))

for alpha in [0.9, 0.8, 0.5, 0.2, 0.1]:

    print(f"decay by {alpha}")
    rb.cmsDecay(key, alpha)
    print(rb.cmsQuery(key, 'foo', 'bar'))


